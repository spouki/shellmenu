#include "../includes/shellmenu.hpp"
#include <iostream>
#include <string>

void shellmenu::newQuestion(std::string toDisplay, int* choices, int nb, void (foo)(void)) {
  std::string vote;
  int choice;
  std::cout << toDisplay << std::endl;
  std::cout << "$_: ";
  std::cin >> vote;
  choice = std::stoi(vote);

  bool isgood = false;
  for (int idx = 0; idx < nb; idx++) {
    if (choice == choices[idx]) {
      isgood = true;
      break;
    }
  }
  if (isgood == false) {
    // Redo ...
    std::cout << "Oups" << std::endl;
  } else {
    foo();
  }
}

shellmenu::shellmenu() {
}

shellmenu::~shellmenu() {
}
